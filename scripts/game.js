﻿var gameArea = {
	canvas : document.getElementById('canvas'),
	width : this.canvas.width,
	height : this.canvas.height,
	context : this.canvas.getContext("2d"),
	bgColor : "LightGray",
	clear : function() {
		this.context.fillStyle = this.bgColor;
		this.context.fillRect(0, 0, this.width, this.height);
	}
};

var frameNumber = 0;
var score = 0;
var firstClick = true;
var gameOver = false;

var player;
var obstacles = [];

function Player() {
	this.height = 20;
	this.width = 20;
	this.color = "red";
	
	this.acceleration = 0;
	this.maxAccel = 10;
	this.minAccel = -10;
	
	this.speed = 1;
	
	this.y = Math.floor(gameArea.height / 2) - Math.floor(this.height / 2);
	this.x = 20;
	
	this.update = function() {
		var futurePos = this.y - this.acceleration;
		if(futurePos >= 0 && futurePos + this.height <= gameArea.height) {
			this.y = futurePos;
		} else if(futurePos < 0) {
			this.y = 0;
		} else {
			this.y = gameArea.height - this.height;
		}
		if(this.acceleration > this.minAccel) {
			this.acceleration--;
		}
	}
	
	this.draw = function() {
		gameArea.context.fillStyle = this.color;
		gameArea.context.fillRect(this.x, this.y, this.width, this.height);
	}
	
	this.checkCollision = function(obstacle) {
        if(obstacle.active && this.x < obstacle.x + obstacle.width && this.x + this.width > obstacle.x && (this.y < obstacle.heightTop || this.y + this.height > obstacle.heightBottom)) {
			return true;
		}
		return false;
    }
}

function Obstacle() {
	this.active = true;
	
	this.width = 10;
	this.color = "blue";
	
	this.minGap = 150;
	this.maxGap = 200;
	
	this.heightTop = Math.floor(Math.random() * (gameArea.height - this.maxGap));
	this.gap = Math.floor(Math.random() * (this.maxGap - this.minGap) + this.minGap);
	this.heightBottom = this.heightTop + this.gap;
	
	this.x = gameArea.width - this.width;
	
	this.passed = false;
	this.spawnedNext = false;
	
	this.update = function() {
		if(this.active) {
			if(this.x > 0) {
				this.x -= player.speed;
			} else {
				this.active = false;
			}
			
			if(!this.passed && this.x < player.x) {
				player.speed += 0.2;
				this.passed = true;
			}
		}
	}
	
	this.move = function(n) {
		this.x = gameArea.width - this.width - n;
	}
	
	this.draw = function() {
		if(this.active) {
			gameArea.context.fillStyle = this.color;
			gameArea.context.fillRect(this.x, 0, this.width, this.heightTop)
			gameArea.context.fillRect(this.x, this.heightBottom, this.width, gameArea.height);
		}
	}
}

function drawText(txt, x, y, size, font, color) {
	gameArea.context.font = size + "px " + font;
	gameArea.context.fillStyle = color;
	gameArea.context.fillText(txt, x, y);
}

function update() {
	gameArea.clear();
	frameNumber++;
	
	if(!gameOver) {
		if(frameNumber == 1) {
			obstacles.push(new Obstacle());
		} else {
			var o = obstacles[obstacles.length - 1];
			if(!o.spawnedNext && o.x < gameArea.width - 200) {
				o.spawnedNext = true;
				obstacles.push(new Obstacle());
			}
		}
		player.update();
		player.draw();
		for (var i = 0; i < obstacles.length; i ++) {
			obstacles[i].update();
			obstacles[i].draw();
			if(player.checkCollision(obstacles[i])) {
				gameOver = true;
			}
		}
		drawText(score, 10, 30, 30, "Courier", "black");
		score++;
	} else {
		drawText("GAME OVER", gameArea.width / 4, gameArea.height / 3, 50, "Courier", "black");
		drawText("Your score: " + score, gameArea.width / 8, gameArea.height * 2 / 3, "Courier", "black");
	}
}

function startGame() {
	player = new Player();
	obstacles.push(new Obstacle());
	obstacles[0].move(200);
	update();
	window.addEventListener('keypress', jump, false);
}

function jump(e) {
	if(e.keyCode == 32 || e.keyCode == 0) {
		e.preventDefault();
		player.acceleration = player.maxAccel;
		if(firstClick) {
			firstClick = false;
			update();
			setInterval(update, 20);
		}
	}
}