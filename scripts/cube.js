/* Stworzenie widoku 3D */
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(75, 1, 0.1, 1000);

var renderer = new THREE.WebGLRenderer();
renderer.setSize(500, 500);
document.querySelector("#cubeDiv").appendChild(renderer.domElement);
renderer.domElement.classList.add('cubes');

function render() {
		requestAnimationFrame( render );
		renderer.render( scene, camera );
}
var controls = new THREE.OrbitControls(camera, renderer.domElement);
controls.addEventListener('change', render);
render();

/* Klasa punktu */
function Point(x, y, z) {
	this.x = Number(x);
	this.y = Number(y);
	this.z = Number(z);
	
	this.string = function() {
		return "[" + this.x + ", " + this.y + ", " + this.z + "]";
	}
}

/* Klasa szescianu dla widoku 3D */
function Cube(center, width) {
	this.geometry = new THREE.BoxGeometry(width, width, width);
	this.material = new THREE.MeshBasicMaterial({ color: Math.random() * 0xffffff });
	this.cube = new THREE.Mesh(this.geometry, this.material);
	this.cube.position.set(center.x, center.y, center.z);
	scene.add(this.cube);
}

/* Rekurencyjne znajdowanie punktow po podanych osiach na podstawie punktu w rogu */
function recursiveCubes(point, amount, width, x, y, z) {
	var ret = [];
	var p;
	if(amount > 0) {
		var r = recursiveCubes(point, amount - 1, width, x, y, z);
		if(r.length > 0) {
			ret = ret.concat(r);
		}
		
		if(x != 0) {
			ret.push(new Point(point.x + (x * amount * width), point.y, point.z));
		}
		if(y != 0) {
			ret.push(new Point(point.x, point.y + (y * amount * width), point.z));
		}
		if(z != 0) {
			ret.push(new Point(point.x, point.y, point.z + (z * width * amount)));
		}
	}
	return ret;
}

/* Znajduje punkt w rogu a potem zwraca punkty na krawedziach dla danego rogu */
function cubesForVertex(point, n, width, x, y, z) {
	var p = new Point(point.x + x * width / 2 + x * width / (2 * n), point.y + y * width / 2 + y * width / (2 * n), point.z + z * width / 2 + z * width / (2 * n));
	return recursiveCubes(p, n, width / n, -x, -y, -z);
}

/* Funkcja glowna */
function cubes() {
	// pobranie danych wejsciowych
	var input = {
		x : document.getElementById('x').value,
		y : document.getElementById('y').value,
		z : document.getElementById('z').value,
		w : document.getElementById('w').value,
		n : document.getElementById('n').value
	};
	
	// znajdowanie punktow na krawedziach
	var center = new Point(input.x, input.y, input.z);
	var cubes = cubesForVertex(center, input.n, input.w, -1, -1, -1);
	cubes = cubes.concat(cubesForVertex(center, input.n, input.w, 1, 1, -1));
	cubes = cubes.concat(cubesForVertex(center, input.n, input.w, 1, -1, 1));
	cubes = cubes.concat(cubesForVertex(center, input.n, input.w, -1, 1, 1));
	
	// rysowanie szescianow
	new Cube(center, input.w);
	cubes.forEach(function(p) {
		list.innerHTML = list.innerHTML + "<li>" + p.string() + "</li>";
		new Cube(p, input.w / input.n);
	});
	
	camera.position.set(center.x + 5, center.y + 5, center.z + 5);
	camera.lookAt(new THREE.Vector3(center.x, center.y, center.z));
	controls.target.set(center.x, center.y, center.z);
}