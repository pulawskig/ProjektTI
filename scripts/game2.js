var gameArea = {
	canvas : document.getElementById('canvas'),
	width : this.canvas.width,
	height : this.canvas.height,
	context : this.canvas.getContext("2d"),
	bgColor : "LightGray",
	clear : function() {
		//this.context.clearRect(0, 0, this.width, this.height);
		this.context.fillStyle = this.bgColor;
		this.context.fillRect(0, 0, this.width, this.height);
	}
};

var frameNumber = 0;
var score = 0;
var gameOver = false;
var firstClick = true;

var cols = 12;
var rows = 12;

var obstacles = [];
var player;
var ball;

var colors = [ "#7E0D81", "#C8116B", "#E54128", "#FD690D", "#FF9536", "#FFA823", "#FEDB00", "#B2D11E", "#088074", "#037D95", "#034885", "#001E67" ];

gameArea.context.strokeStyle = "Black";

function Obstacle(posx, posy, price, color) {
	this.x = posx;
	this.y = posy;
	this.price = price;
	this.color = color;
	
	this.active = true;
	
	this.width = gameArea.width / cols;
	this.height = 10;
	
	this.draw = function() {
		gameArea.context.fillStyle = this.color;
		gameArea.context.fillRect(this.x, this.y, this.width, this.height);
		gameArea.context.strokeRect(this.x, this.y, this.width, this.height);
	}
}

function Player() {
	this.width = 80;
	this.height = 10;
	
	this.active = true;
	
	this.x = gameArea.width / 2 - this.width / 2;
	this.y = gameArea.height - 50;
	
	this.body = new Obstacle(this.x, this.y, 0, "Black");
	this.body.width = this.width;
	this.body.height = this.height;
	
	var self = this;
	
	this.left = function(a) {
		if(this.x - a >= 0) {
			this.x -= a;
			this.body.x -= a;
		} else {
			this.x = 0;
			this.body.x = 0;
		}
	}
	
	this.right = function(a) {
		if(this.x + this.width + a <= gameArea.width) {
			this.x += a;
			this.body.x += a;
		} else {
			this.x = gameArea.width - this.width;
			this.body.x = gameArea.width - this.width;
		}
	}
	
	this.keyEvent = function(e) {
		if(firstClick && (e.keyCode == 32 || e.keyCode == 0)) {
			firstClick = false;
			update();
			setInterval(update, 20);
		} else if(e.keyCode == 37) {
			self.left(5);
		} else if(e.keyCode == 39) {
			self.right(5);
		}
	}
	
	this.draw = function() {
		self.body.draw();
	}
}

function Ball() {
	this.size = 10;
	
	this.width = this.size;
	this.height = this.size;
	
	this.x = player.x + player.width / 2;
	this.y = player.y - this.size;
	
	this.color = "Black";
	
	this.speed = 2;
	
	this.direction = 1;
	
	this.update = function() {
		if(this.y >= gameArea.height) {
			gameOver = true;
		}
		
		if(this.x <= 0 || this.x + this.width >= gameArea.width) {
			this.direction = this.direction + ((this.direction % 2) * 2) - 1;
		}
		
		if(this.y <= 0) {
			this.direction = 5 - this.direction;
		}
		
		switch(this.direction) {
			case 1:
				this.x += this.speed;
				this.y -= this.speed;
				break;
			case 2:
				this.x -= this.speed;
				this.y -= this.speed;
				break;
			case 3:
				this.x -= this.speed;
				this.y += this.speed;
				break;
			case 4:
				this.x += this.speed;
				this.y += this.speed;
				break;
		}
	}
	
	this.draw = function() {
		gameArea.context.fillStyle = this.color;
		gameArea.context.fillRect(this.x, this.y, this.size, this.size);
	}
	
	this.checkCollision = function(obstacle) {
        if(obstacle.active && this.x < obstacle.x + obstacle.width && this.x + this.width > obstacle.x && this.y < obstacle.y + obstacle.height && this.y + this.height > obstacle.y) {
			if(this.y <= obstacle.y - (obstacle.height / 2)) {
				// od dołu
				this.direction = 5 - this.direction;
			} else if(this.y >= obstacle.y + (obstacle.height / 2)) {
				// od góry
				this.direction = 5 - this.direction;
			} else if(this.x < obstacle.x) {
				// od lewej
				this.direction = this.direction + ((this.direction % 2) * 2) - 1;
			} else if(this.x > obstacle.x) {
				// od prawej
				this.direction = this.direction + ((this.direction % 2) * 2) - 1;
			}
			return true;
		}
		return false;
    }
}

function drawText(txt, x, y, size, font, color) {
	gameArea.context.font = size + "px " + font;
	gameArea.context.fillStyle = color;
	gameArea.context.fillText(txt, x, y);
}

function update() {
	gameArea.clear();
	frameNumber++;
	
	if(!gameOver) {
		if(ball.y > player.y + player.height + ball.height) {
			gameOver = true;
		} else if(ball.y > gameArea.height / 2) {
			ball.checkCollision(player.body);
		} else {
			for(var i = 0; i < obstacles.length; i++) {
				if(obstacles[i].active && ball.checkCollision(obstacles[i])) {
					score += obstacles[i].price;
					obstacles[i].active = false;
				}
			}
		}
		
		for(var i = 0; i < obstacles.length; i++) {
			if(obstacles[i].active) {
				obstacles[i].draw();
			}
		}
		
		player.draw();
		
		ball.update();
		ball.draw();
		
		drawText(score, 10, 30, 30, "Courier", "black");
	} else {
		drawText("GAME OVER", gameArea.width / 4, gameArea.height / 3, 50, "Courier", "black");
		drawText("Your score: " + score, gameArea.width / 8, gameArea.height * 2 / 3, "Courier", "black");
	}
}

function startGame() {
	for(var j = 0; j < rows; j++) {
		for(var i = 0; i < cols; i++) {
			obstacles.push(new Obstacle(i * gameArea.width / cols, 50 + (j * 10), 50 * (rows - j), colors[j] ));
		}
	}
	player = new Player();
	ball = new Ball();
	window.addEventListener('keydown', player.keyEvent, false);
	update();
}